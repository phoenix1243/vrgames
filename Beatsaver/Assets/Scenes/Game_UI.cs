using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Game_UI : MonoBehaviour
{
    public TextMeshProUGUI scoreText;
    public Image lifetimeBar;
    
    public static Game_UI instance;
    
    void Awake()
    {
        instance = this;
    }

    public void UpdateScoreText ()
    {
        scoreText.text = string.Format("SCORE/n{0}",GameManager.instance.Score.ToString());
    }
    
    public void UpdatelifetimeBar()
    {
        lifetimeBar.fillAmount = GameManager.instance.lifeTime;
    }
}
