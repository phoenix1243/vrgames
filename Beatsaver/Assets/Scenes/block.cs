using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BlockColor
{

    blue,
    Red

}
public class Block : MonoBehaviour
{

   public BlockColor color;
   
   public GameObject brokenblockLeft;
   public GameObject brokenblockRight;
   public float brokenblockForce;
   public float brokenblockTorque;
   public float brokenblockDestroyDelay;
   

    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("blue_Sword"))
        {
            if(color == BlockColor.blue && GameManager.instance.LeftHandSwordTracker.Velocity.magnitude >= GameManager.instance.swordHitVelocityThreshold)
            {
                //add to the score
            }
            else
            {
                // subtract a life
            }
        }   Hit();
    if(other.CompareTag("red_Sword"))
        {
            if(color == BlockColor.Red && GameManager.instance.RightHandSwordTracker.Velocity.magnitude >= GameManager.instance.swordHitVelocityThreshold)
            {
                //add to the score
            }
            else
            {
                // subtract a life
            }
        }   Hit();
    }
    public void Hit ()
    {
        
        brokenblockLeft.SetActive(true);
        brokenblockRight.SetActive(true);
        
        brokenblockLeft.transform.parent = null;
        brokenblockRight.transform.parent = null;
        
        Rigidbody leftRig = brokenblockLeft.GetComponent<Rigidbody>();
        Rigidbody rightRig = brokenblockRight.GetComponent<Rigidbody>();
        
        leftRig.AddForce(-transform.right * brokenblockForce, ForceMode.Impulse);
        rightRig.AddForce(transform.right * brokenblockForce, ForceMode.Impulse);
    
        
    }
    
}