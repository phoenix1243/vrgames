using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VelocityTracker : MonoBehaviour
{        
    public Transform tracker;
    public Vector3 Velocity;
        
    private Vector3 lastFramePos;
    

    void Update()
    {
        Velocity = (transform.position - lastFramePos) / Time.deltaTime;
        lastFramePos = transform.position;
    }
}
