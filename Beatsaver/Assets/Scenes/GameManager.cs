using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public float startTime = 3.0f;
    public int Score = 0;
    public float lifeTime = 1.0f;
    public int hitBlockScore = 10;
    public float missBlockScore = 0.1f;
    public float wrongBlockScore = 0.08f;
    public float missBlockLife = 0.1f;
    public float wrongBlocklife = 0.08f;
    public float lifeRengenRate = 0.1f;
    public float swordHitVelocityThreshold = 0.5f;
    public float VelocityTracker;
    public float LeftSwordTracker;
    
    
    public VelocityTracker LeftHandSwordTracker;
    public VelocityTracker RightHandSwordTracker;

    public static GameManager instance;

    public void WinGame ()
    {
        SceneManager.LoadScene(0);
    }
    
    public void LoseGame()
    {
        SceneManager.LoadScene(0);
    }
    
    
    void Awake()
    {
        instance = this;
    }
    
    public void Addscore()
    {
        Score = hitBlockScore;
        Game_UI.instance.UpdateScoreText();
    }
    public void Missblock()
    {
        lifeTime -= missBlockLife;
    }
    public void HitWrongBlcokScore()
    {
        lifeTime -= wrongBlocklife;
    }
    
    void update()
    {
        lifeTime = Mathf.MoveTowards(lifeTime, 1.0f, lifeRengenRate * Time.deltaTime);
        
        if(lifeTime <= 0.0f)
            LoseGame();
        
        Game_UI.instance.UpdatelifetimeBar();
    }
    
    
}

